package splash

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const BaseTestAPIURL = "https://test-api.splashpayments.com"

// Client is
type Client struct {
	HTTPClient *http.Client
	BaseURL    string
	APIKey     string
}

// BaseResponse is
type BaseResponse struct {
	Response struct {
		Data    []interface{} `json:"data"`
		Details *DetailsResp  `json:"details"`
		Errors  []*ErrorResp  `json:"errors"`
	} `json:"response"`
}

//DetailsResp is
type DetailsResp struct {
	RequestID int `json:"requestId"`
}

// ErrorResp is
type ErrorResp struct {
	Field    string `json:"field"`
	Code     int    `json:"code"`
	Severity int    `json:"severity"`
	Msg      string `json:"msg"`
}

func (c *Client) postRequest(u string, jsonReq, jsonResp interface{}) error {
	j, err := json.Marshal(jsonReq)
	if err != nil {
		return err
	}

	log.Println("request" + string(j))

	req, err := http.NewRequest("POST", u, bytes.NewReader(j))
	if err != nil {
		return err
	}

	req.Header.Add("APIKEY", c.APIKey)
	req.Header.Add("Content-Type", "application/json")

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	var b bytes.Buffer
	if _, err := io.Copy(&b, resp.Body); err != nil {
		return err
	}
	debug := b.String()

	log.Println(debug)

	if resp.StatusCode != http.StatusOK {
		return &ErrBadStatusCode{
			OriginalBody: debug,
			Code:         resp.StatusCode,
		}
	}

	var payload BaseResponse
	if err := json.NewDecoder(&b).Decode(&payload); err != nil {
		return &ErrNotExpectedJSON{
			OriginalBody: "",
			Err:          err,
		}
	}

	if len(payload.Response.Errors) > 0 {
		return fmt.Errorf("%s %s", payload.Response.Errors[0].Field, payload.Response.Errors[0].Msg)
	}

	stringJSON, err := json.Marshal(payload.Response.Data[0])
	if err != nil {
		return err
	}

	if err := json.NewDecoder(bytes.NewReader(stringJSON)).Decode(jsonResp); err != nil {
		return &ErrNotExpectedJSON{
			OriginalBody: "",
			Err:          err,
		}
	}

	if err := resp.Body.Close(); err != nil {
		return err
	}

	return nil
}

func (c *Client) getRequest(u string, search string, jsonResp interface{}) error {
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return err
	}

	req.Header.Add("APIKEY", c.APIKey)
	req.Header.Add("Content-Type", "application/json")
	if len(search) > 0 {
		req.Header.Add("SEARCH", search)
	}

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var b bytes.Buffer
	if _, err := io.Copy(&b, resp.Body); err != nil {
		return err
	}
	debug := b.String()

	log.Println("DEBUG..................")
	log.Println(debug)

	if resp.StatusCode != http.StatusOK {
		return &ErrBadStatusCode{
			OriginalBody: debug,
			Code:         resp.StatusCode,
		}
	}

	var payload BaseResponse
	if err := json.NewDecoder(&b).Decode(&payload); err != nil {
		return &ErrNotExpectedJSON{
			OriginalBody: "",
			Err:          err,
		}
	}

	if len(payload.Response.Errors) > 0 {
		return fmt.Errorf("%s %s", payload.Response.Errors[0].Field, payload.Response.Errors[0].Msg)
	}

	if len(payload.Response.Data) == 0 {
		return nil
	}

	stringJSON, err := json.Marshal(payload.Response.Data)
	if err != nil {
		return err
	}

	log.Println("JSON.................")
	log.Println(string(stringJSON))

	if err := json.NewDecoder(bytes.NewReader(stringJSON)).Decode(jsonResp); err != nil {
		return &ErrNotExpectedJSON{
			OriginalBody: "",
			Err:          err,
		}
	}

	return nil
}

// String returns a pointer to the string value passed in.
func String(v string) *string {
	return &v
}

// Int returns a pointer to the int value passed in.
func Int(v int) *int {
	return &v
}
