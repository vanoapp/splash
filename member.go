package splash

// MemberReq is
type MemberReq struct {
	ID        *string `json:"id"`
	Created   *string `json:"created"`
	Modified  *string `json:"modified"`
	Creator   *string `json:"creator"`
	Modifier  *string `json:"modifier"`
	Merchant  *string `json:"merchant"`
	Title     *string `json:"title"`
	First     *string `json:"first"`
	Middle    *string `json:"middle"`
	Last      *string `json:"last"`
	Ssn       *string `json:"ssn"`
	DOB       *string `json:"dob"`
	Dl        *string `json:"dl"`
	Dlstate   *string `json:"dlstate"`
	Ownership *string `json:"ownership"`
	Email     *string `json:"email"`
	Fax       *string `json:"fax"`
	Phone     *string `json:"phone"`
	Country   *string `json:"country"`
	Zip       *string `json:"zip"`
	State     *string `json:"state"`
	City      *string `json:"city"`
	Address2  *string `json:"address2"`
	Address1  *string `json:"address1"`
	Primary   *string `json:"primary"`
	Inactive  int     `json:"inactive"`
	Frozen    int     `json:"frozen"`
}

// Member is
type Member struct {
	ID        string      `json:"id"`
	Created   string      `json:"created"`
	Modified  string      `json:"modified"`
	Creator   string      `json:"creator"`
	Modifier  string      `json:"modifier"`
	Merchant  string      `json:"merchant"`
	Title     string      `json:"title"`
	First     string      `json:"first"`
	Middle    string      `json:"middle"`
	Last      string      `json:"last"`
	Ssn       string      `json:"ssn"`
	DOB       string      `json:"dob"`
	Dl        string      `json:"dl"`
	Dlstate   string      `json:"dlstate"`
	Ownership interface{} `json:"ownership"`
	Email     string      `json:"email"`
	Fax       string      `json:"fax"`
	Phone     string      `json:"phone"`
	Country   string      `json:"country"`
	Zip       string      `json:"zip"`
	State     string      `json:"state"`
	City      string      `json:"city"`
	Address2  string      `json:"address2"`
	Address1  string      `json:"address1"`
	Primary   string      `json:"primary"`
	Inactive  int         `json:"inactive"`
	Frozen    int         `json:"frozen"`
}
