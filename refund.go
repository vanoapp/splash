package splash

import (
	"fmt"
	"os"
)

// RefundRequest is
type RefundRequest struct {
	Entry       string  `json:"entry"`
	Description *string `json:"description"`
	Amount      int     `json:"amount"`
}

// RefundResponse is
type RefundResponse struct {
	ID          string  `json:"id"`
	Created     string  `json:"crated"`
	Entry       string  `json:"entry"`
	Description *string `json:"description"`
	Amount      int     `json:"amount"`
}

// CreateRefund is
func (c *Client) CreateRefund(req *RefundRequest) (*RefundResponse, error) {
	u := fmt.Sprintf("%s/refunds", os.Getenv("SPLASH_BASE_URL"))

	var resp RefundResponse
	if err := c.postRequest(u, req, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}
