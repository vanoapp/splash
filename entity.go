package splash

// EntityReq is
type EntityReq struct {
	ID         *string       `json:"id"`
	Created    *string       `json:"created"`
	Modified   *string       `json:"modified"`
	Creator    *string       `json:"creator"`
	Modifier   *string       `json:"modifier"`
	IPCreated  *string       `json:"ipCreated"`
	IPModified *string       `json:"ipModified"`
	ClientIP   *string       `json:"clientIp"`
	Login      *string       `json:"login"`
	Parameter  *string       `json:"parameter"`
	Type       *string       `json:"type"`
	Name       *string       `json:"name"`
	Address1   *string       `json:"address1"`
	Address2   *string       `json:"address2"`
	City       *string       `json:"city"`
	State      *string       `json:"state"`
	Zip        *string       `json:"zip"`
	Country    *string       `json:"country"`
	Phone      *string       `json:"phone"`
	Fax        *string       `json:"fax"`
	Email      *string       `json:"email"`
	Website    *string       `json:"website"`
	Ein        *string       `json:"ein"`
	Currency   *string       `json:"currency"`
	Custom     *string       `json:"custom"`
	Inactive   int           `json:"inactive"`
	Frozen     int           `json:"frozen"`
	Accounts   []*AccountReq `json:"accounts"`
}

// Entity is
type Entity struct {
	ID         string    `json:"id"`
	Created    string    `json:"created"`
	Modified   string    `json:"modified"`
	Creator    string    `json:"creator"`
	Modifier   string    `json:"modifier"`
	IPCreated  string    `json:"ipCreated"`
	IPModified string    `json:"ipModified"`
	ClientIP   string    `json:"clientIp"`
	Login      string    `json:"login"`
	Parameter  string    `json:"parameter"`
	Type       string    `json:"type"`
	Name       string    `json:"name"`
	Address1   string    `json:"address1"`
	Address2   string    `json:"address2"`
	City       string    `json:"city"`
	State      string    `json:"state"`
	Zip        string    `json:"zip"`
	Country    string    `json:"country"`
	Phone      string    `json:"phone"`
	Fax        string    `json:"fax"`
	Email      string    `json:"email"`
	Website    string    `json:"website"`
	Ein        string    `json:"ein"`
	Currency   string    `json:"currency"`
	Custom     string    `json:"custom"`
	Inactive   int       `json:"inactive"`
	Frozen     int       `json:"frozen"`
	Accounts   []Account `json:"accounts"`
}
