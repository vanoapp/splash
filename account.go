package splash

// AccountReq is
type AccountReq struct {
	ID          *string        `json:"id"`
	Created     *string        `json:"created"`
	Modified    *string        `json:"modified"`
	Creator     *string        `json:"creator"`
	Modifier    *string        `json:"modifier"`
	Entity      *string        `json:"entity"`
	Token       *string        `json:"token"`
	Name        *string        `json:"name"`
	Description *string        `json:"description"`
	Primary     *string        `json:"primary"`
	Status      *string        `json:"status"`
	Currency    *string        `json:"currency"`
	Inactive    int            `json:"inactive"`
	Frozen      int            `json:"frozen"`
	Account     BankAccountReq `json:"account"`
}

// BankAccountReq is
type BankAccountReq struct {
	Method  int     `json:"method"`
	Number  *string `json:"number"`
	Routing *string `json:"routing"`
}

// Account is
type Account struct {
	ID          string      `json:"id"`
	Created     string      `json:"created"`
	Modified    string      `json:"modified"`
	Creator     string      `json:"creator"`
	Modifier    string      `json:"modifier"`
	Entity      string      `json:"entity"`
	Token       string      `json:"token"`
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Primary     string      `json:"primary"`
	Status      string      `json:"status"`
	Currency    string      `json:"currency"`
	Inactive    int         `json:"inactive"`
	Frozen      int         `json:"frozen"`
	Account     BankAccount `json:"account"`
}

// BankAccount is
type BankAccount struct {
	Method  int    `json:"method"`
	Number  string `json:"number"`
	Routing string `json:"routing"`
}
