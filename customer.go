package splash

import (
	"fmt"
	"os"
)

// CustomerRequest is
type CustomerRequest struct {
	Merchant *string `json:"merchant"`
	First    string  `json:"first"`
	Last     string  `json:"last"`
	Email    string  `json:"email"`
}

// CustomerResponse is
type CustomerResponse struct {
	ID       string      `json:"id"`
	Created  string      `json:"created"`
	Modified string      `json:"modified"`
	Creator  string      `json:"creator"`
	Modifier string      `json:"modifier"`
	Login    string      `json:"login"`
	Merchant string      `json:"merchant"`
	First    string      `json:"first"`
	Middle   interface{} `json:"middle"`
	Last     string      `json:"last"`
	Company  interface{} `json:"company"`
	Email    string      `json:"email"`
	Fax      interface{} `json:"fax"`
	Phone    interface{} `json:"phone"`
	Country  interface{} `json:"country"`
	Zip      interface{} `json:"zip"`
	State    interface{} `json:"state"`
	City     interface{} `json:"city"`
	Address2 interface{} `json:"address2"`
	Address1 interface{} `json:"address1"`
	Inactive int         `json:"inactive"`
	Frozen   int         `json:"frozen"`
}

// CreateCustomer is
func (c *Client) CreateCustomer(req *CustomerRequest) (*CustomerResponse, error) {
	u := fmt.Sprintf("%s/customers", os.Getenv("SPLASH_BASE_URL"))

	var resp CustomerResponse
	if err := c.postRequest(u, req, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}
