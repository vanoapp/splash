package splash

import (
	"fmt"
	"os"
)

// Merchant is
type Merchant struct {
	ID            string   `json:"id"`
	Entity        Entity   `json:"entity"`
	Created       string   `json:"created"`
	Modified      string   `json:"modified"`
	Creator       string   `json:"creator"`
	Modifier      string   `json:"modifier"`
	LastActivity  string   `json:"lastActivity"`
	Dba           string   `json:"dba"`
	New           string   `json:"new"`
	Established   string   `json:"established"`
	AnnualCCSales int      `json:"annualCCSales"`
	AvgTicket     int      `json:"avgTicket"`
	Amex          string   `json:"amex"`
	Discover      string   `json:"discover"`
	Mcc           string   `json:"mcc"`
	Status        string   `json:"status"`
	Boarded       string   `json:"boarded"`
	TinStatus     string   `json:"tinStatus"`
	TcVersion     string   `json:"tcVersion"`
	TcDate        string   `json:"tcDate"`
	Inactive      int      `json:"inactive"`
	Frozen        int      `json:"frozen"`
	Members       []Member `json:"members"`
}

// FullMerchantRequest is
type FullMerchantRequest struct {
	Entity    EntityReq   `json:"entity"`
	New       *string     `json:"new"`
	Mcc       *string     `json:"mcc"`
	Status    *string     `json:"status"`
	TcVersion *string     `json:"tcVersion"`
	Members   []MemberReq `json:"members"`
}

// MerchantResponse is
type MerchantResponse struct {
	Entity        *Entity     `json:"entity"`
	Members       []Member    `json:"members"`
	ID            string      `json:"id"`
	Created       string      `json:"created"`
	Modified      string      `json:"modified"`
	Creator       string      `json:"creator"`
	Modifier      string      `json:"modifier"`
	LastActivity  string      `json:"lastActivity"`
	Dba           string      `json:"dba"`
	New           string      `json:"new"`
	Established   string      `json:"established"`
	AnnualCCSales int         `json:"annualCCSales"`
	AvgTicket     int         `json:"avgTicket"`
	Amex          string      `json:"amex"`
	Discover      string      `json:"discover"`
	Mcc           string      `json:"mcc"`
	Status        string      `json:"status"`
	Boarded       string      `json:"boarded"`
	TinStatus     string      `json:"tinStatus"`
	TcVersion     interface{} `json:"tcVersion"`
	TcDate        string      `json:"tcDate"`
	Inactive      int         `json:"inactive"`
	Frozen        int         `json:"frozen"`
}

// CreateMerchant is
func (c *Client) CreateMerchant() {

}

// CreateFullMerchant is
func (c *Client) CreateFullMerchant(p *FullMerchantRequest) (*MerchantResponse, error) {
	u := fmt.Sprintf("%s/merchants", os.Getenv("SPLASH_BASE_URL"))

	var resp MerchantResponse
	if err := c.postRequest(u, p, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}
