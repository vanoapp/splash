package splash

import (
	"fmt"
	"os"
)

// TokenRequest is
type TokenRequest struct {
	Customer string   `json:"customer"`
	Payment  *Payment `json:"payment"`
}

// TokenResponse is
type TokenResponse struct {
	Payment struct {
		Method  int         `json:"method"`
		Number  string      `json:"number"`
		Routing string      `json:"routing"`
		Payment interface{} `json:"payment"`
		Bin     string      `json:"bin"`
	} `json:"payment"`
	ID         string `json:"id"`
	Created    string `json:"created"`
	Modified   string `json:"modified"`
	Creator    string `json:"creator"`
	Modifier   string `json:"modifier"`
	Customer   string `json:"customer"`
	Token      string `json:"token"`
	Expiration string `json:"expiration"`
	Inactive   int    `json:"inactive"`
	Frozen     int    `json:"frozen"`
}

// CreateToken is
func (c *Client) CreateToken(req *TokenRequest) (*TokenResponse, error) {
	u := fmt.Sprintf("%s/tokens", os.Getenv("SPLASH_BASE_URL"))

	var resp TokenResponse
	if err := c.postRequest(u, req, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}
