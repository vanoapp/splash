package splash

import (
	"fmt"
	"os"
)

// TransactionRequest is
type TransactionRequest struct {
	Merchant string   `json:"merchant"`
	Type     int      `json:"type"`
	Origin   *int     `json:"origin"`
	Payment  *Payment `json:"payment"`
	Token    *string  `json:"token"`
	Total    int      `json:"total"`
	Batch    *string  `json:"batch"`
	Fortxn   *string  `json:"fortxn"`
}

// Payment is
type Payment struct {
	Method     *int   `json:"method"`
	Number     string `json:"number"`
	Cvv        string `json:"cvv"`
	Expiration string `json:"expiration"`
}

// TransactionResponse is
type TransactionResponse struct {
	Payment struct {
		Method  int         `json:"method"`
		Number  string      `json:"number"`
		Routing string      `json:"routing"`
		Payment interface{} `json:"payment"`
		Bin     string      `json:"bin"`
	} `json:"payment"`
	ID              string      `json:"id"`
	Created         string      `json:"created"`
	Modified        string      `json:"modified"`
	Creator         string      `json:"creator"`
	Modifier        string      `json:"modifier"`
	IPCreated       string      `json:"ipCreated"`
	IPModified      string      `json:"ipModified"`
	Merchant        string      `json:"merchant"`
	Token           interface{} `json:"token"`
	Fortxn          interface{} `json:"fortxn"`
	Batch           string      `json:"batch"`
	Subscription    interface{} `json:"subscription"`
	Type            int         `json:"type"`
	Expiration      string      `json:"expiration"`
	Currency        string      `json:"currency"`
	AuthDate        interface{} `json:"authDate"`
	AuthCode        interface{} `json:"authCode"`
	Captured        interface{} `json:"captured"`
	Settled         interface{} `json:"settled"`
	SettledCurrency interface{} `json:"settledCurrency"`
	SettledTotal    interface{} `json:"settledTotal"`
	AllowPartial    int         `json:"allowPartial"`
	Order           string      `json:"order"`
	Description     interface{} `json:"description"`
	Descriptor      string      `json:"descriptor"`
	Terminal        interface{} `json:"terminal"`
	Origin          int         `json:"origin"`
	Tax             interface{} `json:"tax"`
	Total           int         `json:"total"`
	Cashback        interface{} `json:"cashback"`
	Authorization   string      `json:"authorization"`
	Approved        interface{} `json:"approved"`
	Cvv             int         `json:"cvv"`
	Swiped          int         `json:"swiped"`
	Emv             int         `json:"emv"`
	Signature       int         `json:"signature"`
	Unattended      interface{} `json:"unattended"`
	First           interface{} `json:"first"`
	Middle          interface{} `json:"middle"`
	Last            interface{} `json:"last"`
	Company         interface{} `json:"company"`
	Email           interface{} `json:"email"`
	Address1        interface{} `json:"address1"`
	Address2        interface{} `json:"address2"`
	City            interface{} `json:"city"`
	State           interface{} `json:"state"`
	Zip             interface{} `json:"zip"`
	Country         interface{} `json:"country"`
	Phone           interface{} `json:"phone"`
	Status          interface{} `json:"status"`
	Refunded        int         `json:"refunded"`
	Reserved        interface{} `json:"reserved"`
	CheckStage      string      `json:"checkStage"`
	Inactive        int         `json:"inactive"`
	Frozen          int         `json:"frozen"`
}

// CreateTransaction is
func (c *Client) CreateTransaction(req *TransactionRequest) (*TransactionResponse, error) {
	u := fmt.Sprintf("%s/txns", os.Getenv("SPLASH_BASE_URL"))

	var resp TransactionResponse
	if err := c.postRequest(u, req, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}

// GetTransaction is
func (c *Client) GetTransaction(id string) (*TransactionResponse, error) {
	u := fmt.Sprintf("%s/txns/%s", os.Getenv("SPLASH_BASE_URL"), id)

	var resp []*TransactionResponse
	if err := c.getRequest(u, "", &resp); err != nil {
		return nil, err
	}
	return resp[0], nil
}

// RevokeTransaction is
func (c *Client) RevokeTransaction(txnID, merchantID string, txnType int) (*TransactionResponse, error) {
	u := fmt.Sprintf("%s/txns", os.Getenv("SPLASH_BASE_URL"))

	req := &TransactionRequest{
		Fortxn:   String(txnID),
		Type:     txnType,
		Merchant: merchantID,
	}

	var resp TransactionResponse
	if err := c.postRequest(u, req, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}

// GetTransactions is
func (c *Client) GetTransactions(search string) ([]*TransactionResponse, error) {
	u := fmt.Sprintf("%s/txns", os.Getenv("SPLASH_BASE_URL"))

	var resp []*TransactionResponse
	if err := c.getRequest(u, search, &resp); err != nil {
		return nil, err
	}
	if resp == nil {
		resp = []*TransactionResponse{}
	}
	return resp, nil
}
